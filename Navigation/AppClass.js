/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import {
  View,
  Image,
  Text,
} from 'react-native';
import axios from 'axios';


import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class App extends React.Component{
  
  constructor(props){
    super(props);

    this.state ={
      listNews : [],
    }

  }

  componentDidMount(){
    //getdata form newsapi.org
    this.getListNews();
  }

  getListNews(){
    //getdata from newsapi.org
    axios ({
      method: 'GET',
      url : 'http://newsapi.org/v2/top-headlines?country=id&apiKey=7e5d2238760047dca18491b8a50f72bd',
    }).then((res) => {
      console.info('res', res.data);
      this.setState({listNews:res.data.articles});
      /*
      * res.data = {
        ....status:...
        ....totalResult:...
        ....articles :[],
      }
      */
    }).catch((err)=>{
      console.error(err);
    });
  }

render (){
  return (
    <View>
      <View>
        <Image source={{uri: 'https://klikjatim.com/wp-content/uploads/2020/06/bromo-1.jpg' }}></Image>
        <Text>Judul</Text>
        <Text>Deskripsi</Text>
      </View>
    </View>
  );
};}

export default NewsScreen;
