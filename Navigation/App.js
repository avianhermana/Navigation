import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import NewsScreen from './src/screen/NewsScreen';
import MainNavigator from './src/navigator/MainNavigator';
import AppStack from './src/navigator/AppStack';
import ProfileScreen from './src/screen/ProfileScreen';

function App(){
  return (
  <NavigationContainer>
    <AppStack/>
    {/* <ProfileScreen/> */}
  </NavigationContainer>
  // <NewsScreen/>
  );
}

export default App;