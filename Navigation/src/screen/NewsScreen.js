/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
} from 'react-native';
import axios from 'axios';
import {apiGetListNews} from '../common/news';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

//Define Variable
// const App = () => {  
// };

function NewsScreen () {
  const [listNews, setListNews] = useState([]);

  useEffect(() => {
    //GetData from newsapi.org
    getListNews();
  },[]);

  function getListNews (){
    axios ({
      method: 'GET',
      url : 'http://newsapi.org/v2/top-headlines?country=id&apiKey=8a22f29ea7f447698c5598ec2f86506e',
    })
    //apiGetListNews()
    .then((res) => {
      console.info('banyaknya data', res.data.articles);
      setListNews(res.data.articles);
      /*
      * res.data = {
        ....status:...,
        ....totalResult:...,
        ....articles :[],
      }
      */
    }).catch((err)=>{
        console.error(JSON.stringify(err));
    });
  }

  return (
    <View>
    <View>
        <Text style={{fontWeight:"bold", fontSize:24,textAlign:"center" }}>
            Berita Hari Ini
        </Text>
    </View>
    <ScrollView>
      {/* {LOOPING DISINI} */}
      {listNews.map((news, index) => (
          <View key={index} style={{borderWidth:3, marginBottom:10}}>
          <Image style={{width:'100%', height:200}}
          source={{uri: news.urlToImage, }}></Image>
          <Text style={{fontWeight:'bold'}}>{news.title}</Text>
          <Text>{news.content}</Text>
        </View>
      ))}
    </ScrollView>
    </View>
  );
};

export default NewsScreen;
