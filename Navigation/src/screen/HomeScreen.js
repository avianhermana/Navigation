import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export default function HomeScreen(props){
    return(
        <View>
            <Text>Home</Text>

            <TouchableOpacity onPress={() => props.navigation.navigate('Detail')}>
                <Text style={{color:'red', fontSize:24}}> Go to detail </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('More')}>
                <Text style={{color:'red', fontSize:24}}> More </Text>
            </TouchableOpacity>
        </View>
    );
}