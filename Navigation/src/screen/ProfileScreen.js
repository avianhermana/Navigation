import React from 'react';
import {View, Text, Image, StyleSheet, Dimensions, TextInput} from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EditProfileScreen from './EditProfileScreen';

const consWidth = Dimensions.get('window').width * 0.8;
const consHeigth = Dimensions.get('window').height;

export default function ProfileScreen(props){
    return(
        
            <View style={styles.container}>
                <ScrollView>
                <View>
                    <View style={styles.cardProfile} >
                        <Image style={styles.logoAvatar} source={require("../assets/Avatar.png")}></Image>
                        
                        <View style={styles.profileName}>
                            <Text style={{fontWeight:'bold'}}>MR. JOESTAR</Text>
                        </View>
                        <View>
                            <Text style={styles.profileDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tincidunt.</Text>
                        </View>
                        <View style={{alignItems:"center"}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('More')}>
                                <Text style={styles.editProfile}>Edit Profile</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box}>
                            <TouchableOpacity><Text style={styles.post}>Post</Text></TouchableOpacity>
                            <TouchableOpacity><Text style={styles.requestMeeting}>Request Meeting</Text></TouchableOpacity>
                        </View>

                        <View style={styles.cardPost}>
                            <Image style={styles.cardPostImage} source={require("../assets/kucing.jpeg")}></Image>
                            <View style={{position:"absolute"}}>
                                <Image style={{width:40,height:40, borderRadius:50, position:"relative", marginTop:10, marginLeft:10, borderColor:'white', borderWidth:2 }} source={require("../assets/si-joni.png")}></Image>
                            </View>
                            <View style={{flexDirection:'row', marginTop:5, marginBottom:5, marginLeft:15}}>
                                <Text>1432 Likes</Text>
                                <Text style={{marginLeft:10}}>21 Comments</Text>
                            </View>
                            <View style={{flexDirection:'row', marginLeft:15}}>
                                <Ionicons name="heart-outline" size={30} color="black"/>
                                <Ionicons style={{marginLeft:40}} name="chatbubble-outline" size={30} color="black"/>
                            </View>
                        </View>

                    </View>
                </View>
                </ScrollView>
            </View>
        
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#F4F4F4",
        marginTop:10,
        //height: (consHeigth)
    },
    logoAvatar:{
        maxWidth: 100,
        maxHeight: 100,
        alignContent:"center",
        marginLeft:'39%',
        marginTop:'10%',
        marginBottom:2,
        backgroundColor:'white',
        borderRadius:50,
    },
    cardProfile:{
        // flex:1,
        backgroundColor:'white',
        borderColor:'grey',
        borderWidth:1,
        borderRadius:25,
        width:400,
        height:'53%',
        marginTop:1,
        alignContent:"center"
    },
    cardPost:{
        // flex:1,
        backgroundColor:'white',
        // borderColor:'grey',
        borderWidth:1,
        borderRadius:25,
        width:400,
        height:500,
        marginTop:1,
        marginBottom:100,
        alignContent:"center"
    },
    cardPostImage:{
        // flex:1,
        // backgroundColor:'white',
        // borderColor:'black',
        // borderWidth:1,
        borderRadius:24,
        width:400,
        // height:'65%',
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    box:{
        // flex:1,
        flexDirection:'row',
        backgroundColor:'white',
        borderColor:'black',
        width:'100%',
        height:56,
        marginTop:30,
        alignContent:"center",
        marginBottom:20,
    },
    post:{
        width:200,
        marginTop:'8%',
        color:'#FF65C5',
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold"
    },
    requestMeeting:{
        width:200,
        marginTop:'8%',
        textAlign:"center",
        fontSize:18,
        color:'#C4C4C4'
    },
    editProfile:{
        width:200,
        marginTop:40,
        color:'#FF65C5',
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold"
    },
    profileName:{
        fontWeight:"bold",
        alignItems:"center",
        fontSize:24,
        marginBottom:20
    },
    profileDescription:{
        width:250,
        textAlign:"center",
        marginLeft:'20%'
    },
})