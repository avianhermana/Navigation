import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import WeatherItem from '../components/WeatherItem.component';

export default class NewsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listNews: [
        {
          id: 1,
          location: 'location: DKI Jakarta',
          note: 'short_note: Diperkirakan macet',
          suhu: 'suhu: 30°C',
          kelembapan: 'kelembaban: 90%',
          angin: 'angin: 1km/h',
          presipitasi: 'presipitasi: 5%',
        },
        {
          id: 2,
          location: 'location: Semarang',
          note: 'short_note: Diperkirakan hujan',
          suhu: 'suhu: 28°C',
          kelembapan: 'kelembaban: 50%',
          angin: 'angin: 3km/h',
          presipitasi: 'presipitasi: 8%',
        },
        {
          id: 3,
          location: 'location: Pati',
          note: 'short_note: Sebagian besar berawan',
          suhu: 'suhu: 26°C',
          kelembapan: 'kelembaban: 90%',
          angin: 'angin: 5km/h',
          presipitasi: 'presipitasi: 11%',
        },
        {
          id: 4,
          location: 'location: Yogyakarta',
          note: 'short_note: Diperkirakan hujan',
          suhu: 'suhu: 25°C',
          kelembapan: 'kelembaban: 95%',
          angin: 'angin: 5km/h',
          presipitasi: 'presipitasi: 11%',
        },
      ],
    };
  }

  render() {
    return (
      <View>
        <View>
          <Text style={styles.header}>GLINTS WEATHER</Text>
        </View>

        <View>
          {this.state.listNews.map((news, index) => (
            <WeatherItem
              key={index}
              location={news.location}
              note={news.note}
              suhu={news.suhu}
              kelembapan={news.kelembapan}
              angin={news.angin}
              presipitasi={news.presipitasi}></WeatherItem>
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
