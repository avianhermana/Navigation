import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class WeatherItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.itemContent}>
          <View>
            <Text style={styles.location}>{this.props.location}</Text>
          </View>

          <View>
            <Text>
              {this.props.note}
            </Text>
          </View>

          <View>
            <Text>{this.props.suhu}</Text>
          </View>

          <View>
            <Text>{this.props.kelembapan}</Text>
          </View>

          <View>
            <Text>{this.props.angin}</Text>
          </View>

          <View>
            <Text>{this.props.presipitasi}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    padding: 10,
  },
  itemImage: {
    width: 100,
    height: 100,
  },
  itemContent: {
    padding: 10,
  },
  title: {
    fontWeight: 'bold',
  },
});
