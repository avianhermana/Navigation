/* eslint-disable no-extra-semi */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import NewsScreen from './src/screens/NewsScreen';

function App () {
	return (
		<>
	  	<StatusBar barStyle="dark-content" />
	    <SafeAreaView>
	    	<NewsScreen />
	    </SafeAreaView>
	  </>
	);
};

export default App;
